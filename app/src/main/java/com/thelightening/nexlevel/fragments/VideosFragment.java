package com.thelightening.nexlevel.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thelightening.nexlevel.R;
import com.thelightening.nexlevel.activities.VideoCoursesActivity;
import com.thelightening.nexlevel.adapters.VideosSubjectAdapter;
import com.thelightening.nexlevel.models.SubjectDetails;

import java.util.ArrayList;
import java.util.List;

public class VideosFragment extends BaseToolbarFragment {

    RecyclerView subjectHolder;
    List<SubjectDetails> subjectsList;
    VideosSubjectAdapter videosSubjectAdapter;

    public VideosFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_videos, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        subjectHolder = (RecyclerView) view.findViewById(R.id.subject_holder);
        subjectHolder.setHasFixedSize(true);
        subjectHolder.setLayoutManager(new GridLayoutManager(getActivity(), 2, RecyclerView.VERTICAL, false));

        subjectsList = new ArrayList<>();
        subjectsList.add(new SubjectDetails("Anatomy", 10, R.drawable.img_hermatology));
        subjectsList.add(new SubjectDetails("Physiology", 10, R.drawable.img_medical));
        subjectsList.add(new SubjectDetails("Biochemistry", 10, R.drawable.img_pathology));
        subjectsList.add(new SubjectDetails("Microbiology", 10, R.drawable.img_pelvis));
        subjectsList.add(new SubjectDetails("Pathology", 10, R.drawable.img_pathology));
        subjectsList.add(new SubjectDetails("Pharmacology", 10, R.drawable.img_medical));
        subjectsList.add(new SubjectDetails("Forensic Medicine & Toxicology ", 10, R.drawable.img_hermatology));
        subjectsList.add(new SubjectDetails("ENT", 10, R.drawable.img_pelvis));
        subjectsList.add(new SubjectDetails("Opthalmology", 10, R.drawable.img_hermatology));
        subjectsList.add(new SubjectDetails("Preventive and Social Medicine", 10, R.drawable.img_pelvis));

        videosSubjectAdapter = new VideosSubjectAdapter(getActivity(), subjectsList, new VideosSubjectAdapter.OnItemClickCallBack() {
            @Override
            public void onItemClick(int position) {
                Intent intent = new Intent(getActivity(), VideoCoursesActivity.class);
                intent.putExtra("subject_name", subjectsList.get(position).getSubjectName());
                startActivity(intent);
            }
        });

        subjectHolder.setAdapter(videosSubjectAdapter);
    }
}
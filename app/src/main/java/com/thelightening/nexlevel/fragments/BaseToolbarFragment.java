package com.thelightening.nexlevel.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.thelightening.nexlevel.R;
import com.thelightening.nexlevel.adapters.interfaces.ActionCallback;
import com.thelightening.nexlevel.backend.BackendAPIs;
import com.thelightening.nexlevel.backend.RetrofitInterface;
import com.thelightening.nexlevel.backend.interceptors.OfflineException;
import com.thelightening.nexlevel.dialogs.LoadingDialog;
import com.thelightening.nexlevel.localstorage.LocalStorageData;

import java.lang.ref.WeakReference;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

public class BaseToolbarFragment  extends Fragment {

    public BroadcastReceiver fragmentReceiver;

    private View layoutView;

    private ViewSwitcher viewSwitcher;
    private ProgressBar loading_indicator;
    private FloatingActionButton reload_indicator;
    private TextView loading_message;
    private TextView no_data_message;

    private LoadingDialog loadingDialog;

    public RetrofitInterface retrofitInterface;
    private LocalStorageData localStorageData;
    private BackendAPIs backendApi;


    public MyHandler myHandler;

    public BaseToolbarFragment(){}

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        layoutView = view;
        myHandler = new MyHandler(getActivity());
        retrofitInterface = getBackendApi().baseAdapter().create(RetrofitInterface.class);

    }

    public void registerFragmentReceiver(final String fragmentReceiverName){
        if ( fragmentReceiver == null ){
            IntentFilter intentFilter = new IntentFilter(fragmentReceiverName);
            fragmentReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    try{
                        onFragmentReceiverMessage(fragmentReceiverName, intent);
                    }catch (Exception e){   e.printStackTrace();    }
                }
            };
            getActivity().registerReceiver(fragmentReceiver, intentFilter);  //registering receiver
        }
    }

    public void onFragmentReceiverMessage(String activityReceiverName, Intent receivedData){}

    //===== TOAST METHODS
    public void showToastMessage(String message){
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    public LocalStorageData getLocalStorageData() {
        if ( localStorageData == null ){
            localStorageData = new LocalStorageData(getActivity().getApplicationContext());
        }
        return localStorageData;
    }

    public BackendAPIs getBackendApi() {
        if ( backendApi == null ){
            backendApi = new BackendAPIs(getActivity());
        }
        return backendApi;
    }

    //======= Dialogs

    public LoadingDialog getLoadingDialog() {
        closeLoadingDialog();
        if (loadingDialog == null){
            loadingDialog = new LoadingDialog(getActivity());
        }
        return loadingDialog;
    }

    public void closeLoadingDialog(){
        if ( loadingDialog != null ){
            loadingDialog.closeDialog();
        }
    }

    //====================

    @Override
    public void onDestroy() {
        super.onDestroy();
        closeLoadingDialog();
//        closeCustomDialog();

        if ( fragmentReceiver != null ){
            getActivity().unregisterReceiver(fragmentReceiver);
        }
    }

    public void hideSoftKeyboard(View view){
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public void showSoftKeyboard(){
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInputFromWindow(view.getWindowToken(), InputMethodManager.SHOW_FORCED, 0);
        }
    }



    /**
     * Instances of static inner classes do not hold an implicit
     * reference to their outer class.
     */
    public static class MyHandler extends Handler {
        private final WeakReference<Activity> mActivity;
        MyHandler(Activity activity) {
            mActivity = new WeakReference<Activity>(activity);
        }
    }

    public void showError(int statusCode, Throwable t, String message){
        if (getActivity() != null){
            if (statusCode == 503){
                showToastMessage(getActivity().getApplicationContext().getResources().getString(R.string.servers_under_maintainance));
            }
            else if (t instanceof OfflineException || t instanceof ConnectException
                    || t instanceof UnknownHostException){
                showToastMessage(getActivity().getApplicationContext().getResources().getString(R.string.no_internet));
            }
            else if (t instanceof SocketTimeoutException){
                showToastMessage(getActivity().getApplicationContext().getResources().getString(R.string.time_out));
            }
            else {
                showToastMessage(message);
            }
        }
    }


    /*Handle Server Errors*/

    public void showServerError(Throwable t){
        if (t instanceof OfflineException || t instanceof ConnectException){
            displayServerErrorAlert("No internet connection");
        }
        else if (t instanceof SocketTimeoutException){
            displayServerErrorAlert("Connection timeout");
        }
        else {
            displayServerErrorAlert("Something went wrong. Please try again later");
        }
    }

    public void displayServerErrorAlert(String message){
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void displayErrorDialog(String message, final ActionCallback callBack){
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        callBack.onPositiveButtonClick();
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}

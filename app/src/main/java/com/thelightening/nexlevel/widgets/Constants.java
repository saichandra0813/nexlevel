package com.thelightening.nexlevel.widgets;

public class Constants {


    public static final String VIDEO_SKIP_LIST_FRAGMENT = "VIDEO_SKIP_LIST";
    public static final String VIDEO_NOTES_FRAGMENT = "VIDEO_NOTES";
    public static final String VIDEO_SLIDES_FRAGMENT = "VIDEO_SLIDES";

}

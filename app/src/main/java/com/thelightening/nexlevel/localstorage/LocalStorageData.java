package com.thelightening.nexlevel.localstorage;

import android.content.Context;

import com.google.gson.Gson;
import com.pddstudio.preferences.encrypted.EncryptedPreferences;
import com.thelightening.nexlevel.backend.models.ResponseLogin;
import com.thelightening.nexlevel.models.UserDetails;

public class LocalStorageData {

    private EncryptedPreferences sharedPreferences;
    private EncryptedPreferences.EncryptedEditor editor;

    public LocalStorageData(Context context) {
        sharedPreferences = new EncryptedPreferences.Builder(context).withEncryptionPassword("@@telekha##").build();
        editor = sharedPreferences.edit();
    }

    /*
     * Store Response Login
     * */
    public void storeAccessToken(ResponseLogin responseLogin, String mail, String password, boolean rememberUser ){
        if (responseLogin != null){
            String at = "Bearer " + responseLogin.getAccess_token();
            editor.putString("at", at);
            editor.putString("mail", mail);
            editor.putString("password",password);
            editor.putBoolean("rememberUser", rememberUser);
            editor.commit();
        }
    }

    public String getmail(){
        return sharedPreferences.getString("mail", "");
    }

    public String getPassword(){
        return sharedPreferences.getString("password", "");
    }

    public String getAccessToken(){
        return sharedPreferences.getString("at", "");
    }

    public boolean rememberUser(){
        return sharedPreferences.getBoolean("rememberUser", false);
    }

    public void clearAccessToken(){

        editor.putString("at","");
        editor.putString("mail","");
        editor.putString("password","");
        editor.putBoolean("rememberUser", false);
        editor.commit();
    }

    /*
     * Store SignUp Email
     * */
    public void storeSignUpNumber(String mobileNumber){
        if(mobileNumber!= null){
            editor.putString("mobileNumber", mobileNumber);
            editor.commit();
        }
    }

    public String getSignUpNumber(){
        String email = sharedPreferences.getString("mobileNumber", "");
        return email;
    }

    public void clearSignUpEmail(){
        editor.putString("mobileNumber","");
        editor.commit();
    }

    /*
     * Store UserDetails
     * */
    public void storeUserDetails(UserDetails userDetails){
        if(userDetails!= null){
            editor.putString("user_details", new Gson().toJson(userDetails));
            editor.commit();
        }
    }

    public UserDetails getUserDetails(){
        String admin_details_json = sharedPreferences.getString("user_details", "");
        return new Gson().fromJson(admin_details_json, UserDetails.class);
    }

    public void clearUserDetails(){
        editor.putString("user_details","");
        editor.commit();
    }


}

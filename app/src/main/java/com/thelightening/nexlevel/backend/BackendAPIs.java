package com.thelightening.nexlevel.backend;


import android.content.Context;
import android.net.ConnectivityManager;

import com.thelightening.nexlevel.BuildConfig;
import com.thelightening.nexlevel.backend.interceptors.RequestInterceptor;
import com.thelightening.nexlevel.backend.methods.NullOnEmptyConverterFactory;
import com.thelightening.nexlevel.localstorage.LocalStorageData;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BackendAPIs {

    private static Retrofit retrofit;
    private Context context;
    public LocalStorageData localStorageData;

//    public static final String base_url = "http://54.172.113.125:8090/";
    public static final String base_url = "http://100.26.247.127:8090/";

    public BackendAPIs(Context context) {
        this.context = context;
        this.localStorageData = new LocalStorageData(context);
    }

    public LocalStorageData getLocalStorageData(){
        if (localStorageData == null){
            localStorageData = new LocalStorageData(context);
        }
        return localStorageData;
    }

    public Retrofit accessTokenAdapter(){


        return retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(base_url)
                .client(getOAuthHeaders())
                .build();

    }

    public Retrofit signUpAdapter(){
        return new Retrofit.Builder()
                .baseUrl(base_url)
                .addConverterFactory(new NullOnEmptyConverterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .client(getSignUpHeader())
                .build();
    }

    public Retrofit baseAdapter(){

        return new Retrofit.Builder()
                .baseUrl(base_url)
                .addConverterFactory(new NullOnEmptyConverterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .client(getHeaders())
                .build();
    }

    private OkHttpClient getOAuthHeaders() {

        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(20, TimeUnit.SECONDS);
        builder.readTimeout(20, TimeUnit.SECONDS);

        builder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                Request modified = original.newBuilder()
                        .addHeader("Accept", "application/json")
                        .addHeader("Authorization", "Basic bmV4dGxldmVsY2xpZW50OjEyMzQ1Ng==")
                        .build();
                return chain.proceed(modified);
            }
        });

        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        builder.addInterceptor(new RequestInterceptor(connectivityManager));

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        if (BuildConfig.DEBUG){
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(interceptor);
        }
        else {
            interceptor.setLevel(HttpLoggingInterceptor.Level.NONE);
            builder.addInterceptor(interceptor);
        }

        return builder.build();

    }

    private OkHttpClient getSignUpHeader(){

        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(20, TimeUnit.SECONDS);
        builder.readTimeout(20, TimeUnit.SECONDS);
        builder.retryOnConnectionFailure(true);
        builder.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(final Chain chain) throws IOException {

                Request original = chain.request();
                Request modifiedRequest = original.newBuilder()
                        .addHeader("Accept", "application/json")
                        .addHeader("Connection", "Keep-Alive")
                        .build();

                return chain.proceed(modifiedRequest);
            }
        });

        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        builder.addInterceptor(new RequestInterceptor(connectivityManager));

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        if (BuildConfig.DEBUG){
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(logging);
        }
        else {
            logging.setLevel(HttpLoggingInterceptor.Level.HEADERS);
            builder.addInterceptor(logging);
        }

        return builder.build();
    }

    public OkHttpClient getHeaders(){
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(20, TimeUnit.SECONDS);
        builder.readTimeout(20, TimeUnit.SECONDS);

        builder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                Request modified = original.newBuilder()
                        .addHeader("Accept", "application/json")
                        .addHeader("Authorization", localStorageData.getAccessToken())
                        .build();
                return chain.proceed(modified);
            }
        });

        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        builder.addInterceptor(new RequestInterceptor(connectivityManager));

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        if (BuildConfig.DEBUG){
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(interceptor);
        }
        else {
            interceptor.setLevel(HttpLoggingInterceptor.Level.NONE);
            builder.addInterceptor(interceptor);
        }

        return builder.build();
    }
}

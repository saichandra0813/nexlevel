package com.thelightening.nexlevel.backend.models;

/**
 * Created by telekha on 10/12/16.
 */
public class ResponseLogin {

    private String access_token;
    private String token_type;
    private long expires_in;
    private String scope;

    public String getAccess_token() {
        return access_token;
    }

    public String getToken_type() {
        return token_type;
    }

    public long getExpires_in() {
        return expires_in;
    }

    public String getScope() {
        return scope;
    }
}

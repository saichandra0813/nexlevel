package com.thelightening.nexlevel.backend.models;

import java.util.List;

public class ResponseStates {

    private String id;
    private String country;
    private String state;
    private List<String> states;
    private List<String> cities;
    private List<String> college;
    private String updateTS;
    private String createdTS;

    public String getId() {
        return id;
    }

    public String getCountry() {
        return country;
    }

    public List<String> getStates() {
        return states;
    }

    public String getUpdateTS() {
        return updateTS;
    }

    public String getCreatedTS() {
        return createdTS;
    }
}

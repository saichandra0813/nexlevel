package com.thelightening.nexlevel.backend.interceptors;

import java.io.IOException;

public class OfflineException extends IOException {

    @Override
    public String getMessage() {
        return "No connectivity exception";
    }
}

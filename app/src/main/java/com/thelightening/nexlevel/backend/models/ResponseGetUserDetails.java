package com.thelightening.nexlevel.backend.models;

public class ResponseGetUserDetails<T> {
    int status;
    String message;
    private T body;


    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getBody() {
        return body;
    }

    public void setBody(T body) {
        this.body = body;
    }

}

package com.thelightening.nexlevel.backend.models;

import java.util.List;

public class ResponseCities {

    private String id;
    private String countryname;
    private String statename;
    private List<String> cities;
    private String updateTS;
    private String createdTS;

    public String getId() {
        return id;
    }

    public String getCountryname() {
        return countryname;
    }

    public String getStatename() {
        return statename;
    }

    public List<String> getCities() {
        return cities;
    }

    public String getUpdateTS() {
        return updateTS;
    }

    public String getCreatedTS() {
        return createdTS;
    }
}

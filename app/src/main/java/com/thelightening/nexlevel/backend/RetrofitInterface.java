package com.thelightening.nexlevel.backend;

import com.google.gson.JsonObject;
import com.thelightening.nexlevel.backend.models.RequestChangePassword;
import com.thelightening.nexlevel.backend.models.RequestCreateStudent;
import com.thelightening.nexlevel.backend.models.RequestGetCities;
import com.thelightening.nexlevel.backend.models.RequestGetColleges;
import com.thelightening.nexlevel.backend.models.ResponseCities;
import com.thelightening.nexlevel.backend.models.ResponseCollege;
import com.thelightening.nexlevel.backend.models.ResponseGetUserDetails;
import com.thelightening.nexlevel.backend.models.ResponseLogin;
import com.thelightening.nexlevel.backend.models.ResponseObject;
import com.thelightening.nexlevel.backend.models.ResponseStates;
import com.thelightening.nexlevel.models.CourseDetails;
import com.thelightening.nexlevel.models.SubjectDetails;
import com.thelightening.nexlevel.models.UserDetails;
import com.thelightening.nexlevel.models.VideoDetails;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface RetrofitInterface {

    /*  =====   Access Token Integration     =======*/
    @FormUrlEncoded
    @POST("oauth/token")
    Call<ResponseLogin> login(@FieldMap Map<String, Object> requestLogin);

    @GET("api/v1/appUser/getUserDetails")
    Call<ResponseGetUserDetails<UserDetails>> getUserDetails();

    @POST("api/v1/appUser/create")
    Call<ResponseObject<UserDetails>> signUp(@Body RequestCreateStudent requestCreateStudent);

    @POST("api/v1/appUser/resendCode")
    Call<ResponseObject<UserDetails>> resendCode(@Body RequestCreateStudent createStudent);

    @POST("api/v1/appUser/verifyCode")
    Call<ResponseObject<UserDetails>> verifyCode(@Body RequestCreateStudent createStudent);

    @POST("api/v1/appUser/profileUpdate")
    Call<ResponseObject<UserDetails>> updateStudentDetails(@Body RequestCreateStudent requestCreateStudent);

    @POST("api/v1/appUser/forgetPassword")
    Call<JsonObject> forgotPassword(@Body RequestCreateStudent createStudent);

    @POST("api/v1/appUser/changePassword")
    Call<JsonObject> changePassword(@Body RequestChangePassword changePassword);

    @POST("api/v1/appUser/resetPassword")
    Call<JsonObject> resetPassword(@Body RequestChangePassword requestChangePassword);

    @GET("api/v1/place/getStates/{country}")
    Call<ResponseStates> getStates(@Path("country") String country);

    @POST("api/v1/place/getCities")
    Call<ResponseCities> getCities(@Body RequestGetCities getCities);

    @POST("api/v1/place/getColleges")
    Call<List<ResponseCollege>> getColleges(@Body RequestGetColleges getColleges);


    /*VIDEOS MODULE*/
    @GET("api/v1/subject/getAllSubjects")
    Call<ResponseObject<List<SubjectDetails>>>getAllSubjects();

    @GET("api/v1/subject/getCourseBySubId/{subjectId}")
    Call<ResponseObject<List<CourseDetails>>>getCoursesBySubjectId(@Path("subjectId") String subjectId);

    @GET("api/v1/subject/getCourseVideosByCourseId/{courseId}")
    Call<ResponseObject<List<VideoDetails>>> getCourseVideos(@Path("courseId")String courseId);

    @GET("api/v1/subject/getCourseVideoByVideoId/{videoId}")
    Call<ResponseObject>getVideoDetails(@Path("videoId")String videoId);

}

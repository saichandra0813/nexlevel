package com.thelightening.nexlevel.backend.models;

public class RequestChangePassword {

    String mobileNumber;
    String oldPassword;
    String newPassword;

    public RequestChangePassword(String mobileNumber, String oldPassword, String newPassword) {
        this.mobileNumber = mobileNumber;
        this.oldPassword = oldPassword;
        this.newPassword = newPassword;
    }
}

package com.thelightening.nexlevel.backend.models;

public class RequestGetCities {

    private String country;
    private String state;

    public RequestGetCities(String country, String state) {
        this.country = country;
        this.state = state;
    }
}

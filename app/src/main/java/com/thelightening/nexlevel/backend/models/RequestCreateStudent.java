package com.thelightening.nexlevel.backend.models;

public class RequestCreateStudent {

    private String email;
    private String mobileNumber;
    private String password;
    private String role;
    private String fullname;
    private String country;
    private String state;
    private String city;
    private String college;
    private String studentType;
    private String code;


    public RequestCreateStudent(String mobileNumber) {
        this.mobileNumber = email;
    }

    public RequestCreateStudent(String email, String mobileNumber, String password, String fullname) {
        this.email = email;
        this.mobileNumber = mobileNumber;
        this.password = password;
        this.role = "ROLE_STUDENT";
        this.fullname = fullname;
    }

    public RequestCreateStudent(String mobileNumber, String country, String state, String city, String college, String studentType) {
        this.mobileNumber = mobileNumber;
        this.country = country;
        this.state = state;
        this.city = city;
        this.college = college;
        this.studentType = studentType;
    }


    public RequestCreateStudent(String mobileNumber, String code) {
        this.mobileNumber = mobileNumber;
        this.code = code;
    }
}

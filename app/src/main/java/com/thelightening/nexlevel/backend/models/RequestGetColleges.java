package com.thelightening.nexlevel.backend.models;

public class RequestGetColleges {

    private String city;
    private String state;

    public RequestGetColleges(String state, String city) {
        this.state = state;
        this.city = city;
    }
}

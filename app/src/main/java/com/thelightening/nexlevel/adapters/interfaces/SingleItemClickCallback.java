package com.thelightening.nexlevel.adapters.interfaces;

public interface SingleItemClickCallback {

    public void onItemClick(int position);
}

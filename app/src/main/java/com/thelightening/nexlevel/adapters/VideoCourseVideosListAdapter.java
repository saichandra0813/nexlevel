package com.thelightening.nexlevel.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.thelightening.nexlevel.R;
import com.thelightening.nexlevel.models.SubjectDetails;
import com.thelightening.nexlevel.models.VideoDetails;

import java.util.List;

public class VideoCourseVideosListAdapter extends RecyclerView.Adapter<VideoCourseVideosListAdapter.ViewHolder> {

    private Context context;
    private List<VideoDetails> data;
    private OnItemClickCallBack callBack;



    public VideoCourseVideosListAdapter(Context context, List<VideoDetails> list, OnItemClickCallBack callback) {
        this.context = context;
        this.data = list;
        this.callBack = callback;
    }

    public interface OnItemClickCallBack{
        public void onItemClick(int position);
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @NonNull
    @Override
    public VideoCourseVideosListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.view_video_item, parent, false);
        return new VideoCourseVideosListAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull VideoCourseVideosListAdapter.ViewHolder holder, final int position) {
        if(data.get(position)!= null){
            holder.textTitle.setText(data.get(position).getTitle());
            holder.textDuration.setText(data.get(position).getDuration());

            holder.viewItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callBack.onItemClick(position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        CardView viewItem;
        TextView textTitle, textDuration;

        public ViewHolder(View itemView) {
            super(itemView);

            viewItem = (CardView) itemView.findViewById(R.id.item_view);
            textTitle = (TextView) itemView.findViewById(R.id.tv_video_title);
            textDuration = (TextView) itemView.findViewById(R.id.tv_duration);
        }
    }
}

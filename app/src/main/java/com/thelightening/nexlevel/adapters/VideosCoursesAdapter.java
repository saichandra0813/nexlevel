package com.thelightening.nexlevel.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.thelightening.nexlevel.R;
import com.thelightening.nexlevel.models.CourseDetails;
import com.thelightening.nexlevel.models.SubjectDetails;

import java.util.List;

public class VideosCoursesAdapter extends RecyclerView.Adapter<VideosCoursesAdapter.ViewHolder> {

    private Context context;
    private List<CourseDetails> data;
    private OnItemClickCallBack callBack;



    public VideosCoursesAdapter(Context context, List<CourseDetails> list, OnItemClickCallBack callback) {
        this.context = context;
        this.data = list;
        this.callBack = callback;
    }

    public interface OnItemClickCallBack{
        public void onItemClick(int position);
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @NonNull
    @Override
    public VideosCoursesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.view_course_item, parent, false);
        return new VideosCoursesAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull VideosCoursesAdapter.ViewHolder holder, final int position) {
        if(data.get(position)!= null){
            holder.textTitle.setText(data.get(position).getCourseName());

            if(data.get(position).isFree()){
                holder.imageCourseType.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_lock_open_white));
            }else{
                holder.imageCourseType.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_lock_closed_white));
            }

            holder.viewItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callBack.onItemClick(position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        CardView viewItem;
        TextView textTitle, textVideoCount;
        ImageView  imageCourseType;

        public ViewHolder(View itemView) {
            super(itemView);

            viewItem = (CardView) itemView.findViewById(R.id.item_view);
            textTitle = (TextView) itemView.findViewById(R.id.tv_course_name);
            textVideoCount = (TextView) itemView.findViewById(R.id.tv_video_count);
            imageCourseType = (ImageView) itemView.findViewById(R.id.iv_course_type);

        }
    }
}

package com.thelightening.nexlevel.adapters.interfaces;

public interface ActionCallback {

    void onPositiveButtonClick();
}

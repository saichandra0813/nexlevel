package com.thelightening.nexlevel.adapters;

import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.thelightening.nexlevel.fragments.SkipListFragment;
import com.thelightening.nexlevel.fragments.VideoNotesFragment;
import com.thelightening.nexlevel.fragments.VideoSlidesFragment;
import com.thelightening.nexlevel.widgets.Constants;

import java.util.ArrayList;
import java.util.List;

public class TabsFragmentAdapter extends FragmentPagerAdapter {

    private String[] itemsList;
    private List<String> titles = new ArrayList<>();
    private Fragment mCurrentFragment;

    public TabsFragmentAdapter(FragmentManager childFragmentManager,
                               String[] tabItems, List<String> tabNames) {
        super(childFragmentManager);
        this.itemsList = tabItems;
        this.titles = tabNames;
    }

    public Fragment getCurrentFragment() {
        return mCurrentFragment;
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        if (getCurrentFragment() != object) {
            mCurrentFragment = ((Fragment) object);
        }
        super.setPrimaryItem(container, position, object);
    }

    @Override
    public Fragment getItem(int position) {
        if ( itemsList[position].equalsIgnoreCase(Constants.VIDEO_NOTES_FRAGMENT) ){
            return new VideoNotesFragment();
        }
        else if ( itemsList[position].equalsIgnoreCase(Constants.VIDEO_SKIP_LIST_FRAGMENT) ){
            return new SkipListFragment();
        }else if ( itemsList[position].equalsIgnoreCase(Constants.VIDEO_SLIDES_FRAGMENT) ){
            return new VideoSlidesFragment();
        }
        return null;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles.get(position);
    }

    @Override
    public int getCount() {
        return itemsList.length;
    }


}

package com.thelightening.nexlevel.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.thelightening.nexlevel.R;
import com.thelightening.nexlevel.models.SubjectDetails;

import java.util.List;

public class VideosSubjectAdapter extends RecyclerView.Adapter<VideosSubjectAdapter.ViewHolder> {

    private Context context;
    private List<SubjectDetails> data;
    private OnItemClickCallBack callBack;



    public VideosSubjectAdapter(Context context, List<SubjectDetails> list, OnItemClickCallBack callback) {
        this.context = context;
        this.data = list;
        this.callBack = callback;
    }

    public interface OnItemClickCallBack{
        public void onItemClick(int position);
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @NonNull
    @Override
    public VideosSubjectAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.view_videos_subject_item, parent, false);
        return new VideosSubjectAdapter.ViewHolder(v);
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    @Override
    public void onBindViewHolder(@NonNull VideosSubjectAdapter.ViewHolder holder, final int position) {
        if(data.get(position)!= null){
            holder.textTitle.setText(data.get(position).getSubjectName());
            holder.imageTitleLogo.setImageDrawable(context.getResources().getDrawable(data.get(position).getResourceId()));
            holder.textCourseCount.setText(data.get(position).getCoursesCount() + " Courses");

            holder.viewItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callBack.onItemClick(position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        CardView viewItem;
        TextView textTitle, textCourseCount;
        ImageView imageTitleLogo;

        public ViewHolder(View itemView) {
            super(itemView);

            viewItem = (CardView) itemView.findViewById(R.id.view_item);
            textTitle = (TextView) itemView.findViewById(R.id.tv_title);
            textCourseCount = (TextView) itemView.findViewById(R.id.tv_course_count);
            imageTitleLogo = (ImageView) itemView.findViewById(R.id.iv_title_logo);

        }
    }
}

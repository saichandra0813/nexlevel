package com.thelightening.nexlevel.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import androidx.recyclerview.widget.RecyclerView;

import com.thelightening.nexlevel.R;

import java.util.List;

public class BottomListAdapter extends RecyclerView.Adapter<BottomListAdapter.ViewHolder> {

    private Context context;
    private List<String> itemList;
    private CallBack callBack;
    private String type;
    Drawable defDrawable;


    public BottomListAdapter(Context context1, String type, List<String> items, CallBack callBack) {
        this.context = context1;
        this.type = type;
        this.itemList = items;
        this.callBack = callBack;
        if(!TextUtils.isEmpty(type)){
            if(type.contentEquals("country") || type.contentEquals("state") || type.contentEquals("city")){

                defDrawable = context.getResources().getDrawable(R.drawable.ic_location).mutate();

            }else if(type.contentEquals("college")){

                defDrawable = context.getResources().getDrawable(R.drawable.ic_college).mutate();

            }else if(type.contentEquals("type")){

                defDrawable = context.getResources().getDrawable(R.drawable.ic_person).mutate();

            }
        }
    }

    public interface CallBack{
        void onItemClick(int position);
    }

 /*   @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        parentRecycler = recyclerView;
    }*/

    @Override
    public BottomListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.view_spinner_item, parent, false);
        return new BottomListAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(BottomListAdapter.ViewHolder holder, final int position) {

        if(holder!= null){
            if(!TextUtils.isEmpty(itemList.get(position))){
                holder.textTitle.setText(itemList.get(position));
                holder.textTitle.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callBack.onItemClick(position);
                    }
                });
            }

            holder.imageIcon.setImageDrawable(defDrawable);
        }

    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        View itemView;
        ImageView imageIcon;
        TextView textTitle;

        public ViewHolder(View itemView) {
            super(itemView);
            itemView = itemView.findViewById(R.id.item_view);
            imageIcon = (ImageView) itemView.findViewById(R.id.iv_list_icon);
            textTitle = (TextView) itemView.findViewById(R.id.tv_title);
        }
    }
}

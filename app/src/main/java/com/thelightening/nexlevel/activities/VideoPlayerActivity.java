package com.thelightening.nexlevel.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.EditText;
import android.widget.MediaController;
import android.widget.VideoView;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;
import com.thelightening.nexlevel.R;
import com.thelightening.nexlevel.adapters.TabsFragmentAdapter;
import com.thelightening.nexlevel.widgets.Constants;

import java.util.ArrayList;
import java.util.List;

public class VideoPlayerActivity extends BaseActivity{

    TabLayout tabLayout;
    ViewPager viewPager;
    AppBarLayout appBarLayout;

    TabsFragmentAdapter tabsFragmentAdapter;

    private List<String> tabNames = new ArrayList<>();
    private String []tabItems = new String[]{Constants.VIDEO_SKIP_LIST_FRAGMENT, Constants.VIDEO_SLIDES_FRAGMENT,
            Constants.VIDEO_NOTES_FRAGMENT};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_player);

        VideoView videoView =(VideoView)findViewById(R.id.video_view);
        //Set MediaController  to enable play, pause, forward, etc options.
        MediaController mediaController= new MediaController(this);
        mediaController.setAnchorView(videoView);
        //Location of Media File
        Uri uri = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.video);
        //Starting VideView By Setting MediaController and URI
        videoView.setMediaController(mediaController);
        videoView.setVideoURI(uri);
        videoView.requestFocus();
        videoView.start();


        tabNames.add("Video");
        tabNames.add(" Slides ");
        tabNames.add(" Notes ");

        appBarLayout = (AppBarLayout)  findViewById(R.id.appBarLayout);
        tabLayout = (TabLayout) findViewById(R.id.tl_work_loc);
        viewPager = (ViewPager) findViewById(R.id.vp_work_location);

        setUpViewPager();
    }

    private void setUpViewPager() {

        if (tabLayout.getTabCount() > 0){
            tabLayout.removeAllTabs();
        }

        tabLayout.setTabMode(TabLayout.MODE_FIXED);

        tabsFragmentAdapter = new TabsFragmentAdapter(getSupportFragmentManager(), tabItems, tabNames);
        viewPager.setAdapter(tabsFragmentAdapter);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        viewPager.setCurrentItem(0);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {}

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }
}
package com.thelightening.nexlevel.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

import com.thelightening.nexlevel.R;
import com.thelightening.nexlevel.adapters.VideosCoursesAdapter;
import com.thelightening.nexlevel.models.CourseDetails;

import java.util.ArrayList;
import java.util.List;

public class VideoCoursesActivity extends BaseActivity {

    RecyclerView itemsHolder;
    List<CourseDetails> items = new ArrayList<>();
    VideosCoursesAdapter coursesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_courses);


        setupActionBar();
        setToolbarTitle("Courses");

        itemsHolder = (RecyclerView) findViewById(R.id.item_holder);
        itemsHolder.setHasFixedSize(true);
        itemsHolder.setLayoutManager(new LinearLayoutManager(this,  RecyclerView.VERTICAL, false));

        items.add(new CourseDetails("Chapter 1", "10 Videos", true));
        items.add(new CourseDetails("Chapter 2", "13 Videos", true));
        items.add(new CourseDetails("Chapter 3", "5 Videos", false));
        items.add(new CourseDetails("Chapter 4", "2 Videos", false));
        items.add(new CourseDetails("Chapter 5", "7 Videos", false));
        items.add(new CourseDetails("Chapter 6", "19 Videos", false));
        items.add(new CourseDetails("Chapter 7", "1 Video", false));
        items.add(new CourseDetails("Chapter 8", "12 Videos", false));
        items.add(new CourseDetails("Chapter 9", "9 Videos", false));
        items.add(new CourseDetails("Chapter 10", "4 Videos", false));
        items.add(new CourseDetails("Chapter 11", "6 Videos", false));

        coursesAdapter = new VideosCoursesAdapter(this, items, new VideosCoursesAdapter.OnItemClickCallBack() {
            @Override
            public void onItemClick(int position) {
                if(items.get(position).isFree()){
                    Intent intent = new Intent(VideoCoursesActivity.this, VideosListActivity.class);
                    startActivity(intent);
                }else{
                    Intent intent = new Intent(VideoCoursesActivity.this, PaymentDetailsActivity.class);
                    startActivity(intent);
                }
            }
        });

        itemsHolder.setAdapter(coursesAdapter);
    }
}
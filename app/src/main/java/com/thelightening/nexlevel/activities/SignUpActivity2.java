package com.thelightening.nexlevel.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.JsonObject;
import com.thelightening.nexlevel.R;
import com.thelightening.nexlevel.adapters.BottomListAdapter;
import com.thelightening.nexlevel.adapters.interfaces.ActionCallback;
import com.thelightening.nexlevel.backend.RetrofitInterface;
import com.thelightening.nexlevel.backend.models.RequestCreateStudent;
import com.thelightening.nexlevel.backend.models.RequestGetCities;
import com.thelightening.nexlevel.backend.models.RequestGetColleges;
import com.thelightening.nexlevel.backend.models.ResponseCities;
import com.thelightening.nexlevel.backend.models.ResponseCollege;
import com.thelightening.nexlevel.backend.models.ResponseGetUserDetails;
import com.thelightening.nexlevel.backend.models.ResponseLogin;
import com.thelightening.nexlevel.backend.models.ResponseObject;
import com.thelightening.nexlevel.backend.models.ResponseStates;
import com.thelightening.nexlevel.models.UserDetails;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpActivity2 extends BaseActivity implements View.OnClickListener {

    Button btnSignUp;
    TextView textBack, textCountry, textState, textCity, textCollegeName, textStudentType;
    ImageView imageBack, imageCountryCheck, imageStateCheck, imageCityCheck, imageCollegeCheck, imageStudentType;


    List<String> countryList = new ArrayList<>();
    List<String> stateList = new ArrayList<>();
    List<String> cityList = new ArrayList<>();
    List<String> collegeList = new ArrayList<>();
    List<String> studentTypeList = new ArrayList<>();

    RetrofitInterface retrofitInterface;

    private Map<String, Object> requestMap = new HashMap<>();

    String number= "",country ="", state= "", city= "", college = "", type= "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up2);

        initialiseViews();

//        countryList.add("China");
        countryList.add("India");
//        countryList.add("Australia");

        studentTypeList.add("1st year");
        studentTypeList.add("2nd year");
        studentTypeList.add("3rd year");
        studentTypeList.add("4th year");
        studentTypeList.add("Intern");
        studentTypeList.add("Post Intern");
        studentTypeList.add("PG Aspirant");
        studentTypeList.add("Super Speciality Aspirant");

        retrofitInterface = getBackendAPIs().signUpAdapter().create(RetrofitInterface.class);
        number = getLocalStorageData().getSignUpNumber();

    }

    private void initialiseViews() {

        btnSignUp = (Button) findViewById(R.id.btn_submit);

        textBack = (TextView) findViewById(R.id.tv_back);
        textCountry = (TextView) findViewById(R.id.tv_country);
        textState = (TextView) findViewById(R.id.tv_state);
        textCity = (TextView) findViewById(R.id.tv_city);
        textCollegeName = (TextView) findViewById(R.id.tv_college);
        textStudentType = (TextView) findViewById(R.id.tv_student);

        imageBack = (ImageView) findViewById(R.id.iv_back);
        imageCountryCheck = (ImageView) findViewById(R.id.iv_country_check);
        imageStateCheck = (ImageView) findViewById(R.id.iv_state_check);
        imageCityCheck = (ImageView) findViewById(R.id.iv_city_check);
        imageCollegeCheck = (ImageView) findViewById(R.id.iv_college_check);
        imageStudentType = (ImageView) findViewById(R.id.iv_student_check);

        btnSignUp.setOnClickListener(this);
        textBack.setOnClickListener(this);
        textCountry.setOnClickListener(this);
        textState.setOnClickListener(this);
        textCity.setOnClickListener(this);
        textCollegeName.setOnClickListener(this);
        textStudentType.setOnClickListener(this);
        imageBack.setOnClickListener(this);
        imageCountryCheck.setOnClickListener(this);
        imageStateCheck.setOnClickListener(this);
        imageCityCheck.setOnClickListener(this);
        imageCollegeCheck.setOnClickListener(this);
        imageStudentType.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if(v == imageBack || v == textBack){
            finish();
        }else if(v == imageCountryCheck || v == textCountry){
            state = "";
            city = "";
            college = "";
            textState.setText("");
            textCity.setText("");
            textCollegeName.setText("");
            showBottomSheet("Select Country","country",countryList);

        }else if(v == imageStateCheck || v == textState){
            city = "";
            college = "";
            textState.setText("");
            textCity.setText("");
            textCollegeName.setText("");
            if(!TextUtils.isEmpty(country)){
                getStates();
            }else{
                showToastMessage("Select country");
            }

        }else if (v == imageCityCheck || v == textCity){

            college = "";
            textCity.setText("");
            textCollegeName.setText("");
            if(!TextUtils.isEmpty(country) && !TextUtils.isEmpty(state)){
                getCities();
            }else{
                if(TextUtils.isEmpty(country)){
                    showToastMessage("Select country");
                }
                if(TextUtils.isEmpty(state)){
                    showToastMessage("Select state");
                }
            }

        }else if(v == textCollegeName || v == imageCollegeCheck){
            if(!TextUtils.isEmpty(country) && !TextUtils.isEmpty(state) && !TextUtils.isEmpty(city)){
                getColleges();
            }else{

                if(TextUtils.isEmpty(country)){
                    showToastMessage("Select country");
                }
                if(TextUtils.isEmpty(state)){
                    showToastMessage("Select state");
                }
                if(TextUtils.isEmpty(city)){
                    showToastMessage("Select city");
                }
            }
        }else if (v == imageStudentType || v == textStudentType){
            showBottomSheet("Select Student Type","type", studentTypeList);

        }else if(v == btnSignUp){
            if(!TextUtils.isEmpty(state) && !TextUtils.isEmpty(city) && !TextUtils.isEmpty(college)
                    && !TextUtils.isEmpty(type)){
                showToastMessage(""+country+" "+state+ " "+ city+" "+college+" "+type);
                updateStudentDetails();
            }else{
                if(TextUtils.isEmpty(country)){

                    showToastMessage("Select country");

                }
                else if(TextUtils.isEmpty(state)){

                    showToastMessage("Select state");

                }
                if(TextUtils.isEmpty(city)){
                    showToastMessage("Select city");

                }
                if(TextUtils.isEmpty(college)){
                    showToastMessage("Select college");

                }
                if(TextUtils.isEmpty(type)){
                    showToastMessage("Select student type");

                }
            }
        }
    }

    private void showBottomSheet(final String title, final String key, final List<String> itemList) {
        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(SignUpActivity2.this, R.style.MyDialogTheme);
        View sheetView = SignUpActivity2.this.getLayoutInflater().inflate(R.layout.layout_bottom_recycler,
                null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.setCancelable(true);
        TextView textTitle = (TextView) sheetView.findViewById(R.id.title);
        if(!TextUtils.isEmpty(title)){
            textTitle.setText(title);
        }
        RecyclerView itemsHolder = (RecyclerView) sheetView.findViewById(R.id.bottom_holder);
        itemsHolder.setHasFixedSize(true);
        itemsHolder.setLayoutManager(new LinearLayoutManager(SignUpActivity2.this, RecyclerView.VERTICAL,
                false));
        BottomListAdapter listAdapter = new BottomListAdapter(SignUpActivity2.this, key,
                itemList, new BottomListAdapter.CallBack() {
            @Override
            public void onItemClick(int position) {
                if(key.contentEquals("country")){
                    country = itemList.get(position);
                    textCountry.setText(country);
                }else if(key.contentEquals("state")){
                    state = itemList.get(position);
                    textState.setText(state);
                }else if(key.contentEquals("city")){
                    city = itemList.get(position);
                    textCity.setText(city);
                }else if(key.contentEquals("college")){
                    college = itemList.get(position);
                    textCollegeName.setText(college);
                }else if(key.contentEquals("type")){
                    type = itemList.get(position);
                    textStudentType.setText(type);
                }
                mBottomSheetDialog.dismiss();
            }
        });
        itemsHolder.setAdapter(listAdapter);

       /* ImageView btnDismiss = (ImageView) sheetView.findViewById(R.id.iv_dismiss);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
            }
        });*/

        mBottomSheetDialog.show();

    }

    private void getStates(){
        retrofitInterface.getStates(country).enqueue(new Callback<ResponseStates>() {
            @Override
            public void onResponse(Call<ResponseStates> call, Response<ResponseStates> response) {
                if(response.isSuccessful()){
                    if(response.body()!=null){
                        stateList.clear();
                        stateList .addAll(response.body().getStates());
                        showBottomSheet("Select States","state", stateList);
                    }else{
                        showToastMessage("No states are available");
                    }
                }else{
                    showToastMessage("Try again later");
                }
            }

            @Override
            public void onFailure(Call<ResponseStates> call, Throwable t) {
                call.cancel();
                t.printStackTrace();
                showServerError(t);
            }
        });
    }

    private void getCities(){
        RequestGetCities requestGetCities = new RequestGetCities("India", state);
        retrofitInterface.getCities(requestGetCities).enqueue(new Callback<ResponseCities>() {
            @Override
            public void onResponse(Call<ResponseCities> call, Response<ResponseCities> response) {
                if(response.isSuccessful()){
                    if(response.body()!=null){
                        cityList.clear();
                        cityList .addAll(response.body().getCities());
                        showBottomSheet("Select City","city",cityList);
                    }else{
                        showToastMessage("No cities are available");
                    }
                }else{
                    showToastMessage("Please try after sometime");
                }
            }

            @Override
            public void onFailure(Call<ResponseCities> call, Throwable t) {
                call.cancel();
                t.printStackTrace();
                showServerError(t);
            }
        });
    }

    private void getColleges(){
        RequestGetColleges requestGetColleges = new RequestGetColleges(state, city);
        retrofitInterface.getColleges(requestGetColleges).enqueue(new Callback<List<ResponseCollege>>() {
            @Override
            public void onResponse(Call<List<ResponseCollege>> call, Response<List<ResponseCollege>> response) {
                if(response.isSuccessful()){
                    if(response.body()!=null && response.body().size()>0){
                        collegeList.clear();
                        for(int i =0 ; i<response.body().size(); i++){
                            collegeList.add(response.body().get(i).getCollege());
                        }
                        showBottomSheet("Select College","college", collegeList);
                    }else{
                        showToastMessage("No colleges are available");
                    }
                }else{
                    showToastMessage("Please try after sometime");
                }
            }

            @Override
            public void onFailure(Call<List<ResponseCollege>> call, Throwable t) {
                call.cancel();
                t.printStackTrace();
                showServerError(t);
            }
        });
    }

    private void updateStudentDetails() {

        getLoadingDialog().show("Loading....");
        RequestCreateStudent createStudent = new RequestCreateStudent(number, country, state, city, college, type);

        retrofitInterface.updateStudentDetails(createStudent).enqueue(new Callback<ResponseObject<UserDetails>>() {
            @Override
            public void onResponse(Call<ResponseObject<UserDetails>> call, Response<ResponseObject<UserDetails>> response) {
                if(response.isSuccessful()){
                    if(response.body()!=null){
                        showToastMessage(response.body().getMessage());
                       getLocalStorageData().storeUserDetails(response.body().getData());
                       login(response.body().getBody().getMobileNumber(), response.body().getBody().getPassword());

                    }
                }else{
                    closeLoadingDialog();
                    String message = "", error = "";
                    int code;
                    try {
                        JSONObject jsonObject = new JSONObject(response.errorBody().string());
                        message = jsonObject.getString("message");
                        error = jsonObject.getString("error_description");

                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                    code = response.raw().code();

                    if (code == 400 && message.equals("Bad credentials")){
                        showToastMessage("Invalid Credentials");
                    }
                    if(TextUtils.isEmpty(message) && !TextUtils.isEmpty(error)){
                        showToastMessage(error);
                    }
                    else {
                        showToastMessage("Something went wrong. Please try again later");
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseObject<UserDetails>> call, Throwable t) {
                call.cancel();
                t.printStackTrace();
                showServerError(t);
            }
        });
    }

    private void login(final String mail, final String pass) {

        requestMap.put("username", mail);
        requestMap.put("password", pass);
        requestMap.put("grant_type", "password");
        requestMap.put("scope", "read write");
        requestMap.put("client_secret", "123456");
        requestMap.put("client_id", "nextlevelclient");

        RetrofitInterface retrofitInterface = getBackendAPIs().accessTokenAdapter().create(RetrofitInterface.class);
        retrofitInterface.login(requestMap).enqueue(new Callback<ResponseLogin>() {
            @Override
            public void onResponse(Call<ResponseLogin> call, Response<ResponseLogin> response) {
                if(response.isSuccessful()){
                    getLocalStorageData().storeAccessToken(response.body(),mail, pass, true);
                    Intent intent = new Intent(SignUpActivity2.this, HomeActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);

                } else{
                    closeLoadingDialog();
                    String message = "", error = "";
                    int code;
                    try {
                        JSONObject jsonObject = new JSONObject(response.errorBody().string());
                        error = jsonObject.getString("error");
                        message = jsonObject.getString("message");
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
//                    code = response.raw().code();

                    if (error.equals("invalid_grant")){
                        showToastMessage("Invalid Credentials");
                    }
                    else if(TextUtils.isEmpty(message) && !TextUtils.isEmpty(error)){
                        showToastMessage(error);
                    }
                    else {
                        showToastMessage("Something went wrong. Please try again later");
                    }

                }
            }

            @Override
            public void onFailure(Call<ResponseLogin> call, Throwable t) {
                closeLoadingDialog();
                call.cancel();
                t.printStackTrace();
                showServerError(t);
            }
        });
    }}
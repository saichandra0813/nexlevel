package com.thelightening.nexlevel.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.thelightening.nexlevel.R;
import com.thelightening.nexlevel.adapters.interfaces.ActionCallback;
import com.thelightening.nexlevel.backend.RetrofitInterface;
import com.thelightening.nexlevel.backend.models.ResponseGetUserDetails;
import com.thelightening.nexlevel.backend.models.ResponseLogin;
import com.thelightening.nexlevel.backend.models.ResponseObject;
import com.thelightening.nexlevel.models.UserDetails;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseActivity implements View.OnClickListener {

    Button btnLogin, btnCreateAccount;
    TextView textRememberMe, textForgotPassword;

    EditText editEmail, editPassword;
    ImageView imageEmailCheck, imageRememberMe;

    boolean rememberUser;

    private Map<String, Object> requestMap = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initialiseViews();
    }

    private void initialiseViews() {

        editEmail = (EditText) findViewById(R.id.et_email);
        editPassword = (EditText) findViewById(R.id.et_password);

        imageEmailCheck = (ImageView) findViewById(R.id.iv_email_check);
        imageRememberMe = (ImageView) findViewById(R.id.iv_remember_check);

        textRememberMe = (TextView) findViewById(R.id.tv_remember_me);
        textForgotPassword = (TextView) findViewById(R.id.tv_forgot_password);

        btnCreateAccount = (Button) findViewById(R.id.btn_create_account);
        btnLogin = (Button) findViewById(R.id.btn_login);

        textRememberMe.setOnClickListener(this);
        textForgotPassword.setOnClickListener(this);

        btnLogin.setOnClickListener(this);
        btnCreateAccount.setOnClickListener(this);

        editEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(isValidEmailAddress(charSequence) ){
                    imageEmailCheck.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_check_circle_green));
                }else{
                    imageEmailCheck.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_check_circle_red));
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        rememberUser = false;

    }

    @SuppressLint("UseCompatLoadingForDrawables")
    @Override
    public void onClick(View v) {
        if(v == btnLogin){

            checkValidations();

        }else if(v == btnCreateAccount){
            Intent intent = new Intent(this, SignUpActivity1.class);
            startActivity(intent);
        }else if(v == textRememberMe){
            if(rememberUser){
                rememberUser = false;
                imageRememberMe.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_check_circle_grey));
            }else{
                rememberUser = true;
                imageRememberMe.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_check_circle_green));
            }
        }else if(v == textForgotPassword){
            Intent intent = new Intent(this, ForgotPasswordActivity.class);
            startActivity(intent);
        }
    }

    private void checkValidations() {
        String email = editEmail.getText().toString().trim();
        String pass = editPassword.getText().toString().trim();

        login(email, pass);

        if(!TextUtils.isEmpty(email) && !TextUtils.isEmpty(pass)){
            if(isValidEmailAddress(email) && pass.length()>5){
                login(email, pass);

            }else{
                if(!isValidEmailAddress(email)){
                    showToastMessage("Enter Valid Email / Number");
//                    imageEmailCheck.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_check_circle_red));
                }
                if(pass.length()<=5){
                    showToastMessage("Invalid Password");
                }
            }
        }
    }

    private void login(final String mail, final String pass) {

        getLoadingDialog().show("Please wait, Loading...");
        requestMap.put("username", mail);
        requestMap.put("password", pass);
        requestMap.put("grant_type", "password");
        requestMap.put("scope", "read write");
        requestMap.put("client_secret", "123456");
        requestMap.put("client_id", "nextlevelclient");

        RetrofitInterface retrofitInterface = getBackendAPIs().accessTokenAdapter().create(RetrofitInterface.class);
        retrofitInterface.login(requestMap).enqueue(new Callback<ResponseLogin>() {
            @Override
            public void onResponse(Call<ResponseLogin> call, Response<ResponseLogin> response) {
                if(response.isSuccessful()){
                    getLocalStorageData().storeAccessToken(response.body(),mail, pass, rememberUser);
                    getUserDetails();
                } else{
                    closeLoadingDialog();
                    String message = "", error = "";
                    int code;
                    try {
                        JSONObject jsonObject = new JSONObject(response.errorBody().string());
                        error = jsonObject.getString("error");
                        message = jsonObject.getString("message");
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
//                    code = response.raw().code();

                    if (error.equals("invalid_grant")){
                        showToastMessage("Invalid Credentials");
                    }
                    else if(TextUtils.isEmpty(message) && !TextUtils.isEmpty(error)){
                        if(error.contentEquals("User not verified")){
                            Intent intent = new Intent(LoginActivity.this, SignUpActivity1.class);
                            intent.putExtra("status", "notcompleted");
                            startActivity(intent);
                        }else{
                            showToastMessage(error);
                        }
                    }
                    else {
                        showToastMessage("Something went wrong. Please try again later");
                    }

                }
            }

            @Override
            public void onFailure(Call<ResponseLogin> call, Throwable t) {
                closeLoadingDialog();
                call.cancel();
                t.printStackTrace();
                showServerError(t);
            }
        });
    }

    private void getUserDetails() {
        retrofitInterface.getUserDetails().enqueue(new Callback<ResponseGetUserDetails<UserDetails>>() {
            @Override
            public void onResponse(Call<ResponseGetUserDetails<UserDetails>> call, Response<ResponseGetUserDetails<UserDetails>> response) {
                if(response.isSuccessful()){
                    if(response.body()!= null){
                        if(response.body().getBody().isAccountVerified() && response.body().getBody().isProfileVerified()){
                            getLocalStorageData().storeUserDetails(response.body().getBody());
                            closeLoadingDialog();
                            Intent in = new Intent(LoginActivity.this, HomeActivity.class);
                            startActivity(in);

                            finish();
                        }else{
                            closeLoadingDialog();
                            if(!response.body().getBody().isAccountVerified()){

                                displayErrorDialog("User Not available, Please singup to create account", new ActionCallback() {
                                    @Override
                                    public void onPositiveButtonClick() {
                                        Intent intent = new Intent(LoginActivity.this, SignUpActivity1.class);
                                        intent.putExtra("status", "notcompleted");
                                        startActivity(intent);
                                    }
                                });
                            }else if(!response.body().getBody().isProfileVerified()){
                                getLocalStorageData().storeSignUpNumber(response.body().getBody().getMobileNumber());
                                Intent intent = new Intent(LoginActivity.this, SignUpActivity2.class);
                                startActivity(intent);

                                finish();
                            }
                        }
                    }
                }else{
                    closeLoadingDialog();
                    String message = "", error = "";
                    int code;
                    try {
                        JSONObject jsonObject = new JSONObject(response.errorBody().string());
                        message = jsonObject.getString("message");
                        error = jsonObject.getString("error_description");

                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                    code = response.raw().code();

                    if (code == 400 && message.equals("Bad credentials")){
                        showToastMessage("Invalid Credentials");
                    }
                    if(TextUtils.isEmpty(message) && !TextUtils.isEmpty(error)){
                        showToastMessage(error);
                    }
                    else {
                        showToastMessage("Something went wrong. Please try again later");
                    }

                }
            }

            @Override
            public void onFailure(Call<ResponseGetUserDetails<UserDetails>> call, Throwable t) {
                closeLoadingDialog();
                call.cancel();
                t.printStackTrace();
                showServerError(t);
            }
        });

    }
}
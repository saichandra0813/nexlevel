package com.thelightening.nexlevel.activities;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.thelightening.nexlevel.R;
import com.thelightening.nexlevel.adapters.VideoCourseVideosListAdapter;
import com.thelightening.nexlevel.models.VideoDetails;

import java.util.ArrayList;
import java.util.List;

public class VideosListActivity  extends BaseActivity {

    RecyclerView itemsHolder;
    List<VideoDetails> items = new ArrayList<>();
    VideoCourseVideosListAdapter videosListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_courses);

        setupActionBar();
        setToolbarTitle("Videos");

        itemsHolder = (RecyclerView) findViewById(R.id.item_holder);
        itemsHolder.setHasFixedSize(true);
        itemsHolder.setLayoutManager(new LinearLayoutManager(this,  RecyclerView.VERTICAL, false));

        items.add(new VideoDetails("Video 1", "10:34"));
        items.add(new VideoDetails("Video 2", "5:22"));
        items.add(new VideoDetails("Video 3", "9:08"));
        items.add(new VideoDetails("Video 4", "15:33"));
        items.add(new VideoDetails("Video 5", "16:20"));
        items.add(new VideoDetails("Video 6", "4:00"));
        items.add(new VideoDetails("Video 7", "6:31"));
        items.add(new VideoDetails("Video 8", "10:34"));
        items.add(new VideoDetails("Video 9", "7:10"));
        items.add(new VideoDetails("Video 10", "9:33"));
        items.add(new VideoDetails("Video 11", "11:11"));
        items.add(new VideoDetails("Video 12", "12:12"));

        videosListAdapter = new VideoCourseVideosListAdapter(this, items, new VideoCourseVideosListAdapter.OnItemClickCallBack() {
            @Override
            public void onItemClick(int position) {

                Intent intent = new Intent(VideosListActivity.this, VideoPlayerActivity.class);
                startActivity(intent);
            }
        });

        itemsHolder.setAdapter(videosListAdapter);
    }
}
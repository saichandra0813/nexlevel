package com.thelightening.nexlevel.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.thelightening.nexlevel.R;
import com.thelightening.nexlevel.fragments.ExamsFragment;
import com.thelightening.nexlevel.fragments.HomeFragment;
import com.thelightening.nexlevel.fragments.LiveFragment;
import com.thelightening.nexlevel.fragments.MoreFragment;
import com.thelightening.nexlevel.fragments.VideosFragment;

public class  HomeActivity extends BaseActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    BottomNavigationView bottomNavigationView;

    ImageView toolbarImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        bottomNavigationView = (BottomNavigationView) findViewById(R.id.navigation);

        loadFragment(new HomeFragment());
        bottomNavigationView.setOnNavigationItemSelectedListener(this);

        toolbarImage = (ImageView) findViewById(R.id.toolbar_image);

        Glide.with(this)
                .load("cxz")
                .into(toolbarImage);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment selectedFragment = null;
        switch (item.getItemId()) {
            case R.id.action_home:
                selectedFragment = new HomeFragment();
                break;
            case R.id.action_exams:
                selectedFragment = new ExamsFragment();
                break;
            case R.id.action_live:
                selectedFragment = new LiveFragment();
                break;
            case R.id.action_videos:
                selectedFragment = new VideosFragment();
                break;
            case R.id.action_more:
                selectedFragment = new MoreFragment();
                break;

        }
        return loadFragment(selectedFragment);
    }

    private boolean loadFragment(Fragment fragment) {
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.frame_layout, fragment)
                    .commit();
            return true;
        }
        return false;
    }

}
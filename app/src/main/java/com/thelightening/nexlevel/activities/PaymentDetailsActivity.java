package com.thelightening.nexlevel.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.thelightening.nexlevel.R;

public class PaymentDetailsActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_details);

        setupActionBar();
        setToolbarTitle("Payment Details");
    }
}
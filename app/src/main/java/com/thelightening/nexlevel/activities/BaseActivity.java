package com.thelightening.nexlevel.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.util.Patterns;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.thelightening.nexlevel.R;
import com.thelightening.nexlevel.adapters.interfaces.ActionCallback;
import com.thelightening.nexlevel.backend.BackendAPIs;
import com.thelightening.nexlevel.backend.RetrofitInterface;
import com.thelightening.nexlevel.backend.interceptors.OfflineException;
import com.thelightening.nexlevel.dialogs.LoadingDialog;
import com.thelightening.nexlevel.localstorage.LocalStorageData;

import java.net.ConnectException;
import java.net.SocketTimeoutException;

public class BaseActivity extends AppCompatActivity {

    Context context;
    public Toolbar toolbar;
    public ActionBar actionBar;

    private LoadingDialog loadingDialog;


    public RetrofitInterface retrofitInterface;
    private LocalStorageData localStorageData;
    private BackendAPIs backendAPIs;

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);

        context = getApplicationContext();

        retrofitInterface = getBackendAPIs().baseAdapter().create(RetrofitInterface.class);

    }


    //==============  ACTIONBAR METHODS  ==============//

    protected void setupActionBar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null){
            setSupportActionBar(toolbar);
            /*actionbar.setHomeAsUpIndicator(R.drawable.ic_menu);*/
            actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.setDisplayHomeAsUpEnabled(true);
                actionBar.setDisplayShowHomeEnabled(true);

            }
        }
    }

    public void setToolbarTitle(String title){
        if (toolbar != null){
            actionBar = getSupportActionBar();
            TextView toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_text);
            if (actionBar != null) {
                actionBar.setTitle("");
                toolbarTitle.setText(title);
            }
        }
    }

    public void showToastMessage(String message){
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    /*
     *   Initialize LocalStorage Class
     * */
    public LocalStorageData getLocalStorageData(){
        if (localStorageData == null){
            localStorageData = new LocalStorageData(context);
        }
        return localStorageData;
    }

    /*
     *   Initialize Backened Class
     * */
    public BackendAPIs getBackendAPIs(){
        if (backendAPIs == null){
            backendAPIs = new BackendAPIs(context);
        }
        return backendAPIs;
    }

    //======= Dialogs

    public LoadingDialog getLoadingDialog() {
        closeLoadingDialog();
        if (loadingDialog == null){
            loadingDialog = new LoadingDialog(this);

        }
        return loadingDialog;
    }

    public void closeLoadingDialog(){
        if ( loadingDialog != null ){
            loadingDialog.closeDialog();
        }
    }


    /*
     *   Email Validation
     * */
    public static boolean isValidEmailAddress(CharSequence email) {
        return email != null && (android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches() || Patterns.PHONE.matcher(email).matches());
    }

    public void showServerError(Throwable t){
        if (t instanceof OfflineException || t instanceof ConnectException){
            showToastMessage("No Internet Connection");
        }
        else if (t instanceof SocketTimeoutException){
            showToastMessage("Connection Timeout");
        }
        else {
            showToastMessage("Something went wrong. Please try again later");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        closeLoadingDialog();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void hideSoftKeyboard(){
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }

    public void showSoftKeyboard(){
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.toggleSoftInputFromWindow(view.getWindowToken(), InputMethodManager.SHOW_FORCED, 0);
            }
        }
    }

    public void displayErrorDialog(String message, final ActionCallback callBack){
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        callBack.onPositiveButtonClick();
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}

package com.thelightening.nexlevel.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.thelightening.nexlevel.R;
import com.thelightening.nexlevel.adapters.LaunchPagerAdapter;
import com.thelightening.nexlevel.widgets.ViewPagerIndicator;

public class LaunchScreenActivity extends BaseActivity implements View.OnClickListener {

    TextView textSkip;
    Button btnNext;

    ViewPager viewPager;
    ViewPagerIndicator viewPagerIndicator;

    int[] launch_images;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch_screen);

        initialiseViewPager();

        textSkip = (TextView) findViewById(R.id.tv_skip);
        btnNext = (Button) findViewById(R.id.btn_next);

        textSkip.setOnClickListener(this);
        btnNext.setOnClickListener(this);

        if(!TextUtils.isEmpty(getLocalStorageData().getAccessToken())){
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(LaunchScreenActivity.this, HomeActivity.class);
                    startActivity(intent);
                    finish();
                }
            }, 1500);

        }
    }

    private void initialiseViewPager() {
        viewPager = (ViewPager) findViewById(R.id.home_view_pager);
        viewPagerIndicator = (ViewPagerIndicator) findViewById(R.id.view_pager_indicator);

        launch_images = new int[]{R.drawable.img_pager_screen_1, R.drawable.img_pager_screen_1,
                R.drawable.img_pager_screen_1, R.drawable.img_pager_screen_1};

        LaunchPagerAdapter launchPagerAdapter = new LaunchPagerAdapter(this, launch_images);
        viewPager.setAdapter(launchPagerAdapter);

        viewPagerIndicator.setupWithViewPager(viewPager);
        viewPagerIndicator.addOnPageChangeListener(mOnPageChangeListener);
    }

    @NonNull
    private final ViewPager.OnPageChangeListener mOnPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(final int position, final float positionOffset, final int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(final int position) {
            if (viewPager.getCurrentItem() < launch_images.length - 1) {
                btnNext.setText("Next");
            } else if (viewPager.getCurrentItem() == launch_images.length - 1) {
                btnNext.setText("Login");
            }
        }

        @Override
        public void onPageScrollStateChanged(final int state) {
        }
    };

    @Override
    public void onClick(View v) {
        if (v == textSkip) {

            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            finish();

        } else if (v == btnNext) {
            if (viewPager.getCurrentItem() < launch_images.length - 1) {
                viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
            } else if (viewPager.getCurrentItem() == launch_images.length - 1) {
                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        }
    }
}
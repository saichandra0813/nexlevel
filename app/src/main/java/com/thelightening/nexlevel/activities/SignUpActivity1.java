package com.thelightening.nexlevel.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.thelightening.nexlevel.R;
import com.thelightening.nexlevel.backend.RetrofitInterface;
import com.thelightening.nexlevel.backend.models.RequestCreateStudent;
import com.thelightening.nexlevel.backend.models.ResponseObject;
import com.thelightening.nexlevel.models.UserDetails;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpActivity1 extends BaseActivity implements View.OnClickListener {

    Button btnSignUp, btnSubmit;
    TextView textBack, textLogin, textResendOTP;
    EditText editName, editEMail, editNumber, editPassword, editOTP;
    ImageView imageBack;

    FrameLayout layoutOtpView;

    RetrofitInterface retrofitInterface;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up1);


        initialiseViecws();
        retrofitInterface = getBackendAPIs().signUpAdapter().create(RetrofitInterface.class);

    }

    private void initialiseViecws() {
        layoutOtpView = (FrameLayout) findViewById(R.id.layout_otp);

        btnSignUp = (Button) findViewById(R.id.btn_sign_up);
        btnSubmit = (Button) findViewById(R.id.btn_submit);

        textBack = (TextView) findViewById(R.id.tv_back);
        textLogin = (TextView) findViewById(R.id.tv_login);
        textResendOTP = (TextView) findViewById(R.id.tv_otp_resend);

        editName = (EditText) findViewById(R.id.et_name);
        editEMail = (EditText) findViewById(R.id.et_email);
        editNumber= (EditText) findViewById(R.id.et_number);
        editPassword = (EditText) findViewById(R.id.et_password);
        editOTP = (EditText) findViewById(R.id.et_otp);

        imageBack = (ImageView) findViewById(R.id.iv_back);

        textBack.setOnClickListener(this);
        imageBack.setOnClickListener(this);
        textLogin.setOnClickListener(this);
        textResendOTP.setOnClickListener(this);
        btnSignUp.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if(v == imageBack || v == textBack || v == textLogin){
            finish();
        }else if(v == btnSignUp){
            checkValidations();
        }else if(v == btnSubmit){

            String otp = editOTP.getText().toString().trim();
            if(!TextUtils.isEmpty(otp)&& otp.length() == 4){
                verifyOTP(otp);
            }
        }else if(v == textResendOTP){
            resendOTP();
        }
    }

    private void checkValidations() {

        String name = editName.getText().toString().trim();
        String email = editEMail.getText().toString().trim();
        String number = editNumber.getText().toString().trim();
        String pass = editPassword.getText().toString().trim();

        if(!TextUtils.isEmpty(name) && !TextUtils.isEmpty(email) &&!TextUtils.isEmpty(number) &&
                !TextUtils.isEmpty(pass) && pass.length()>5){
            createStudent(name, email, number, pass);
        }else{
            if(TextUtils.isEmpty(name)){
               showToastMessage("Enter Name");
            }
            if(TextUtils.isEmpty(email)){
                showToastMessage("Enter Email");

            }
            if(TextUtils.isEmpty(number)){
                showToastMessage("Enter Number");

            }
            if(TextUtils.isEmpty(pass) || pass.length()<5){
                showToastMessage("Enter valid Password");
            }
        }
    }

    private void createStudent(String name, String email, String number, String pass) {

        RequestCreateStudent createStudent = new RequestCreateStudent(email, number, pass, name);
        retrofitInterface.signUp(createStudent).enqueue(new Callback<ResponseObject<UserDetails>>() {
            @Override
            public void onResponse(Call<ResponseObject<UserDetails>> call, Response<ResponseObject<UserDetails>> response) {
                if(response.isSuccessful()&& response.body()!= null){
                    if(response.body().getStatus() == 201 || response.body().getStatus() == 204){
                        showToastMessage(response.body().getMessage());
                        layoutOtpView.setVisibility(View.VISIBLE);
                    }else{
                        showToastMessage(response.body().getMessage());
                    }
                }else{
                    showToastMessage("Something went wrong, Please try again later");
                }

            }

            @Override
            public void onFailure(Call<ResponseObject<UserDetails>> call, Throwable t) {

            }
        });
    }

    private void verifyOTP(String pin) {
        getLoadingDialog().show("loading...");
        RequestCreateStudent createStudent = new RequestCreateStudent(editNumber.getText().toString(), pin);
        retrofitInterface.verifyCode(createStudent).enqueue(new Callback<ResponseObject<UserDetails>>() {
            @Override
            public void onResponse(@NonNull Call<ResponseObject<UserDetails>> call, @NonNull Response<ResponseObject<UserDetails>> response) {
                closeLoadingDialog();
                if(response.isSuccessful()){
                    if(response.body().getStatus() == 200){
                        getLocalStorageData().storeSignUpNumber(editNumber.getText().toString().trim());
                        Intent intent = new Intent(SignUpActivity1.this, SignUpActivity2.class);
                        startActivity(intent);
                        finish();

                    }else{

                    }
                }else{
                   showToastMessage(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<ResponseObject<UserDetails>> call, Throwable t) {
                closeLoadingDialog();
                call.cancel();
                t.printStackTrace();
                showServerError(t);
            }
        });
    }

    private void resendOTP() {

        getLoadingDialog().show("Loading...");
        RequestCreateStudent createStudent = new RequestCreateStudent(editNumber.getText().toString());
        retrofitInterface.resendCode(createStudent).enqueue(new Callback<ResponseObject<UserDetails>>() {
            @Override
            public void onResponse(Call<ResponseObject<UserDetails>> call, Response<ResponseObject<UserDetails>> response) {
                if(response.isSuccessful()){
                   if(response.body().getStatus() == 200){
                       showToastMessage(response.body().getMessage());
                   }else{
                       showToastMessage(response.body().getMessage());
                   }
                }else{
                    showToastMessage(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<ResponseObject<UserDetails>> call, Throwable t) {
                closeLoadingDialog();
                call.cancel();
                t.printStackTrace();
                showServerError(t);
            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}
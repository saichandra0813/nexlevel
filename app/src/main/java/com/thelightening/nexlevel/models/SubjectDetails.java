package com.thelightening.nexlevel.models;

public class SubjectDetails {

    String id;
    String subjectName;
    ImageDetails  subjectImage;
    int coursesCount ;
    int resourceId;

    public SubjectDetails(String subjectName, int coursesCount, int resourceId) {
        this.subjectName = subjectName;
        this.coursesCount = coursesCount;
        this.resourceId = resourceId;
    }

    public int getResourceId() {
        return resourceId;
    }

    public void setResourceId(int resourceId) {
        this.resourceId = resourceId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public ImageDetails getSubjectImage() {
        return subjectImage;
    }

    public void setSubjectImage(ImageDetails subjectImage) {
        this.subjectImage = subjectImage;
    }

    public int getCoursesCount() {
        return coursesCount;
    }

    public void setCoursesCount(int coursesCount) {
        this.coursesCount = coursesCount;
    }
}

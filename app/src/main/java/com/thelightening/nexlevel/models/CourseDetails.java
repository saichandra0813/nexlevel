package com.thelightening.nexlevel.models;

public class CourseDetails {

    String id;
    String subjectId;
    String courseName;
    String totalDuration;
    String noOfVideos;
    boolean isFree;


    public CourseDetails(String subjectName, String videosCount, boolean isFree) {
        this.courseName = subjectName;
        this.noOfVideos = videosCount;
        this.isFree = isFree;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getTotalDuration() {
        return totalDuration;
    }

    public void setTotalDuration(String totalDuration) {
        this.totalDuration = totalDuration;
    }

    public String getNoOfVideos() {
        return noOfVideos;
    }
    public void setNoOfVideos(String noOfVideos) {
        this.noOfVideos = noOfVideos;
    }


    public boolean isFree() {
        return isFree;
    }

    public void setFree(boolean free) {
        isFree = free;
    }
}

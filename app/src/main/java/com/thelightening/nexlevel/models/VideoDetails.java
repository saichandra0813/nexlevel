package com.thelightening.nexlevel.models;

public class VideoDetails {

        String id;
        String title;
        ImageDetails thumbnail;
        String duration;

    public VideoDetails(String title, String duration) {
        this.title = title;
        this.duration = duration;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ImageDetails getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(ImageDetails thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }
}

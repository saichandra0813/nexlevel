package com.thelightening.nexlevel.models;

public class ImageDetails {

    String fileId;
    String mimeType;
    String originalFileName;
    String url;

    public ImageDetails(String fileId, String mimeType, String originalFileName, String url) {
        this.fileId = fileId;
        this.mimeType = mimeType;
        this.originalFileName = originalFileName;
        this.url = url;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public String getOriginalFileName() {
        return originalFileName;
    }

    public void setOriginalFileName(String originalFileName) {
        this.originalFileName = originalFileName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
